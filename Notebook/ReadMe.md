The following is a guide to the code and data used for data analysis.

Timeseries bioreactor data is present in the form of CSVs for ODs, dilutions, and single cell fluorescence. The data can be loaded
as a reactordata object. All the functions used for analysis utilise this class. 

The directory contains two different kinds of code. 
The InBioGitRepo provides the libraries and class definitions that are necessary to load the bioreactor data.

The jupyter notebook consists of script and functions that were used to analyse the data once it was loaded.
The code was developed in Windows 10 and verified by running in Window 7.

Depenendencies for the code to run without hiccups,

Python 3.5+

Pandas 1.2.4
Numpy 1.20
Matplotlib 3.4.2
Seaborn 0.11.1
SciPy 1.6.3
SciKit 0.24.2
tkinter 8.6


Courage!
